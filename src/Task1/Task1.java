package Task1;
/**
 * allow to obtain the absolute value of the number that the user input from the console
 */

import java.util.Scanner;
public class Task1 {

        public static void main(String[] args) {
            Scanner a = new Scanner(System.in);
            int b = a.nextInt();

            if (b < 0) {
                int c  = (-1) * b ;
                System.out.println(c);
            }
            else System.out.println(b);
        }
    }
