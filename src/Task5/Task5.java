package Task5;
/**
 * allow to get the greatest common divisor ot two numbers
 */
import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите два целых числа: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        if (a > b) {
            if (b != 0 && (a % b) == 0)
                System.out.println("НОД введенных чисел равен " + b);
        } else {
            int c;
            while (b != 0) {
                c = a % b;
                a = b;
                b = c;
            }
            return;
        }
    }
}