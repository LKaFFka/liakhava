import java.io.IOException;


public class HW10 {
    public static void main(String args[])
            throws java.io.IOException {
        int a, ignore;
        int answer = 75;
        do {
            System.out.println("Задумана цифра из диапозона 1 - 100");
            System.out.print("Попытайтесь ее угадать: ");
            a = (int) System.in.read();
              if (a == answer) System.out.println("Верно");
                    else {
                        System.out.println("К сожалению, Вы не угадали. ");
                        if (a <= 50)
                            System.out.println("Холодно");
                        else System.out.println("Горячо");
                        System.out.println("Повторите попытку: ");
                    }
            }
            while (answer != a);
        }
    }