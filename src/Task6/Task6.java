package Task6;
    /**
     * allow to change the values of numbers
     */
    public class Task6 {
        public static void main(String[] args) {
            int a = 3, b = 10, c;
            c = a;
            a = b;
            b = c;
            System.out.println("a = " + a + ", b = " + b);
        }
    }
/**
 * with XOR (Exclusive Or) operator
 */
//    public static void main(String[] args) {
//        int a = 2, b = 5;
//
//        a = a ^ b;
//        b = a ^ b;
//        a = a ^ b;
//        System.out.println("a = " + a + ", b = " + b);
//
//    }
//}
}
